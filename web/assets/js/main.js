main();
// loadData();
// loadTunel();
// loadAddClient();
// window.addEventListener('load', function() {
//     // loadDataRemove()
// 	// loadTunelRemove();
// 	// loadAddClientRemove();
// });
//  Loading
function loadData() {
	const loadingData = document.getElementById("loading-page-data");
	loadingData.classList.add("loading");
}
function loadDataRemove() {
	const loadingData = document.getElementById("loading-page-data");
	loadingData.classList.remove("loading");
}
function loadAddClient() {
	const loadingAddClient = document.getElementById("loading-page-add-client");
	loadingAddClient.classList.add("loading");
}
function loadAddClientRemove() {
	const loadingAddClient = document.getElementById("loading-page-add-client");
	loadingAddClient.classList.remove("loading");
}
//
function loadTunel() {
	const loadingTunel = document.getElementById("loading-page-tunel");
	loadingTunel.classList.add("loading");
}
function loadTunelRemove() {
	const loadingTunel = document.getElementById("loading-page-tunel");
	loadingTunel.classList.remove("loading");
}
//
function main() {
	getData();
	getTunel();
	AddClient();
}

function debounce(func, delay) {
	let timerId;
	return function (...args) {
		clearTimeout(timerId);
		timerId = setTimeout(() => {
			func.apply(this, args);
		}, delay);
	};
}
//
const listItems = document.querySelectorAll(".sidebar-list li");
listItems.forEach((item) => {
	item.addEventListener("click", () => {
		let isActive = item.classList.contains("active");

		listItems.forEach((el) => {
			el.classList.remove("active");
		});

		if (isActive) item.classList.remove("active");
		else item.classList.add("active");
	});
});

const toggleSidebar = document.querySelector(".toggle-sidebar");
const logo = document.querySelector(".logo-box");
const sidebar = document.querySelector(".sidebar");

toggleSidebar.addEventListener("click", () => {
	sidebar.classList.toggle("close");
});

logo.addEventListener("click", () => {
	sidebar.classList.toggle("close");
});
// User info menu
const userMenu = document.getElementById("user-info");
userMenu.addEventListener("click", () => {
	userMenu.classList.toggle("user-info-active");
	const infoUserMenu = document.getElementById("user-info-menu");
	let isActive = infoUserMenu.classList.contains("user-info-menu-active");
	if (isActive) infoUserMenu.classList.remove("user-info-menu-active");
	else infoUserMenu.classList.add("user-info-menu-active");
});
// --------------------------------------------------------MAIN------------------------------------------------
// Get data
function getData() {
	loadData();
	fetch("../../apis/HandleGetData.php")
		.then((res) => res.json())
		.then((data) => {
			// Get data
			// Debug 
			if (data.success == false) {
				document.getElementById("items-list").innerHTML = data.result;
			} else {
				const { clients, client_config } = JSON.parse(data.result);
				if(!clients || clients.length === 0) {  
					document.getElementById("items-list").innerHTML = `<li>No client Data!</li>`;
					
				} else {
					var renderClients = clients.map((client) =>
					renderItem(client, client_config));
					document.getElementById("items-list").innerHTML =
						renderClients.join("");
					showClientDetails();
					// Search container
					var txtSearch = document.getElementById("txt-search");
					var searchValue = "";
					var clientsData = [];
					txtSearch.addEventListener(
						"keyup",
						debounce(function () {
							loadData();
							searchValue = txtSearch.value;
							if (searchValue.length > 0) {
								clientsData = clients.filter((word) =>
									word.includes(searchValue)
								);
								//
								if (clientsData.length == 0) {
									renderClients = `<p>(╥﹏╥) No client found !</p>`;
								} else {
									renderClients = clientsData.map((client) =>
										renderItem(client, client_config)
									);
								}
							} else {
								renderClients = clients.map((client) =>
									renderItem(client, client_config)
								);
							}
							if (Array.isArray(renderClients)) {
								document.getElementById("items-list").innerHTML =
									renderClients.join("");
								showClientDetails();
								loadDataRemove();
							} else {
								document.getElementById("items-list").innerHTML = renderClients;
								loadDataRemove();
							}
						}, 500)
					);
				}
			}
			loadDataRemove();
		});
}
//======================== Add client========================================
function AddClient() {
	var btnAddClient = document.getElementById("btn-add-client");
	btnAddClient.addEventListener("click", () => {
		var newClientName = document.getElementById("txt-add-client").value;
		var tunelSelected = document.getElementById("tunel-selecter").value;
		// Get the selected option's value
		var clientsExistArr;
		fetch("../../apis/HandleGetClient.php")
			.then((response) => response.text())
			.then((data) => {
				// Get data
				clientsExistArr = data.trim().split("\n");
				var clientExist = clientsExistArr.includes(newClientName);
				if (
					tunelSelected.length < 1 ||
					tunelSelected == "" ||
					tunelSelected == null
				) {
					Toastify({
						text: "No tunel running !!!",
						className: "toastify-warning",
					}).showToast();
				} else if (
					newClientName.length == 0 ||
					newClientName == "" ||
					newClientName == null
				) {
					Toastify({
						text: "The name must not be empty !",
						className: "toastify-warning",
					}).showToast();
				} else if (clientExist) {
					Toastify({
						text: "The name is exist !",
						className: "toastify-warning",
					}).showToast();
				} else {
					const newClient = {
						name: newClientName,
						tunel: tunelSelected,
					};
					
					fetch("../../apis/HandleAddClient.php", {
						method: "POST",
						headers: {
							"Content-Type": "application/json; charset=utf-8",
						},
						body: JSON.stringify(newClient),
					})
						.then((response) => response.json())
						.then((data) => {
							// debug
							if (data.success == true) {
								Toastify({
									text: data.message,
									className: "toastify-success",
								}).showToast();
								setTimeout(() => {
									getData();
									showClientDetails();
								}, 1000);
							} else {
								Toastify({
									text: data.message,
									className: "toastify-warning",
								}).showToast();
							}
						});
				}
			});
	});
}
function getTunel() {
	loadTunel();
	loadAddClient();
	fetch("../../apis/HandleGetTunel.php")
		.then((res) => res.json())
		.then((data) => {
			if (data.success == false) {
				document.getElementById("tunel-selecter-tool-bar").innerHTML = renderTunelSelection(null, data.message);
				document.getElementById("top-tool-bar-left").innerHTML = renderTunelSelection(null, data.message);
				document.getElementById("tunel-selecter").innerHTML = renderTunelSelection(null, data.message);
			} else {
				let { tunels, tunels_active } = data;
				tunels = tunels.split(".conf\n").slice(0, -1);
				// Render tunel filer tools bar
				var renderTunel = tunels.map((tunel) =>
					renderTunelSelection(tunel)
				);
				document.getElementById("tunel-selecter-tool-bar").innerHTML =
					renderTunel.join(""); 
				if (tunels_active == null) { 
					renderTunel = tunels.map((tunel) => renderTunelInterface(tunel));
					document.getElementById("top-tool-bar-left").innerHTML =
						renderTunel.join("");
					document.getElementById("tunel-selecter").innerHTML =
						renderTunelSelection(null);
					startTunel();
				} else {
					// Show tunel
					tunels_active = tunels_active.split("\n").slice(0, -1);
					const sameElement = tunels.filter((item) =>
						tunels_active.includes(item)
					);
					renderTunel = tunels.map((tunel) => {
						var hasMatch = false;

						for (let i = 0; i < sameElement.length; i++) {
							if (sameElement[i] === tunel) {
								hasMatch = true;
								break;
							}
						}
						return renderTunelInterface(
							tunel,
							hasMatch ? "power-active" : "power-off"
						);
					});
					//
					document.getElementById("top-tool-bar-left").innerHTML =
						renderTunel.join("");
					// Show tunel selecter
					var renderTunelsActive = tunels_active.map((tunel) =>
						renderTunelSelection(tunel)
					);
					document.getElementById("tunel-selecter").innerHTML =
						renderTunelsActive.join("");
					startTunel();
				}
			}
			loadTunelRemove(); 
			loadAddClientRemove();
		});
}
//
function showClientDetails() {
	const infoItems = document.querySelectorAll(".items-list .item-info");
	infoItems.forEach((item) => {
		item.addEventListener("click", () => {
			let infoItem = item.classList.contains("item-info-active");

			infoItems.forEach((el) => {
				el.classList.remove("item-info-active");
			});

			if (infoItem) item.classList.remove("item-info-active");
			else item.classList.add("item-info-active");
		});
	});
}
//
function downLoadFileConfig(client) {
	let clientName = { client };
	fetch("../../apis/HandleGetProfile.php", {
		method: "POST",
		headers: {
			"Content-Type": "application/json; charset=utf-8",
		},
		body: JSON.stringify(clientName),
	})
		.then((response) => response.json())
		.then((data) => {
			if (data.success == true) {
				Toastify({
					text: "This file is being downloaded...",
					className: "toastify-success",
				}).showToast();
				setTimeout(function () {
					let content = data.result;
					let file = new File(["\ufeff" + content], data.client + ".conf", {
						type: "text/plain:charset=UTF-8",
					});
					url = window.URL.createObjectURL(file);
					let aTag = document.createElement("a");
					aTag.style = "display: none";
					aTag.href = url;
					aTag.download = file.name;
					aTag.click();
					window.URL.revokeObjectURL(url);
				}, 2000);
			} else {
				Toastify({
					text: data.message,
					className: "toastify-warning",
				}).showToast();
			}
		});
}
//
function renderItem(x, y) {
	let innerhtml = `<li class="item">
		<div class="item-title">
			<span class="material-icons">devices</span>
			<span class="item-name">${x}</span>
		</div>
		<div class="btn-container"> 
			<span class="material-icons btn-remove">power_settings_new</span>
			<form >
				<input type="hidden" name="clientName[]" value="${x}">
				<p onclick="downLoadFileConfig('${x}')" name="btnForm" class="btn-form"><span class="material-icons btn-download">file_download</span></p>
			</form>
			<div class="item-info">
				<span class="material-icons btn-info">priority_high</span>
				<div class="item-details">
					<ul>
						<li>
							<span class="name-detail address">Address: </span>
							<span class="address-value">${y[x]["Address"]}</span>
						</li>
						<li>
							<span class="name-detail privatekey">PrivateKey: </span>
							<p class="privatekey-value">${y[x]["PrivateKey"]}</p>
						</li>
						<li>
							<span class="name-detail publickey">PublicKey: </span>
							<p class="publickey-value">${y[x]["PublicKey"]}</p>
						</li>
						<li>
							<span class="name-detail endpoint">Endpoint: </span>
							<span class="endpoint-value">${y[x]["Endpoint"]}</span>
						</li>
						<li>
							<span class="name-detail allowedips">AllowedIPs: </span>
							<span class="allowedips-value">${y[x]["AllowedIPs"]}</span>
						</li>
						<li>
							<span class="name-detail keepalive">Persistentkeepalive: </span>
							<span class="keepalive-value">${y[x]["Persistentkeepalive"]}</span>
						</li>
					</ul>
				</div>
			</div>
		</div> 
	</li>`;
	return innerhtml;
}
//
function startTunel() {
	const infoItems = document.querySelectorAll(".toggle");
	infoItems.forEach((item) => {
		item.addEventListener("click", () => {
			loadAddClient();
			let itemOff = item.classList.contains("power-off");
			if (itemOff) {
				item.classList.remove("power-off");
				item.classList.add("power-active");
			} else {
				item.classList.add("power-active");
			}
			let tunelObj = {
				tunel:
					item.getAttribute("data-info").length > 0
						? item.getAttribute("data-info")
						: null,
			};
			fetch("../../apis/HandleUpTunel.php", {
				method: "POST",
				headers: {
					"Content-Type": "application/json; charset=utf-8",
				},
				body: JSON.stringify(tunelObj),
			})
				.then((response) => response.json())
				.then((data) => {
					if (data.success == true) {
						Toastify({
							text: data.message,
							className: "toastify-success",
						}).showToast();
						data.reload ? getTunel() : null; 
					} else {
						Toastify({
							text: data.message,
							className: "toastify-warning",
						}).showToast(); 
					}
					loadAddClientRemove();
				});
		});
	});
}
//
function renderTunelInterface(x, clname = "power-off") {
	var innerInterface = `<div class="interface ">
			<div data-info="${x}" class="toggle toggle--push toggle--push--glow ${clname}">
			<input type="checkbox" class="toggle--checkbox" >
			<label class="toggle--btn" for="toggle--push--glow" data-label-on="on"  data-label-off="off"></label>
		</div>
		<p class="interface-name">${x}</p>
		</div>`;
	return innerInterface;
}
//
function renderTunelSelection(x = null, y = "No tunel running !!!") {
	return x == null
		? `<option value="">${y}</option>`
		: `<option value="${x}">${x}</option>`;
}
