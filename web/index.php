<?php
session_start();
if (empty($_SESSION['loggedin'])) {
    header("location:login.php");
    exit();
}
$currentUser =  $_SESSION['loggedin'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> </title>
    <!-- Box Icons  -->
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <!-- Material icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Styles  -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Script -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body>
    <div class="sidebar close">
        <!-- ========== Logo ============  -->
        <a href="#" class="logo-box">
            <img src="assets/img/logo1.jpg" alt="">
            <div class="logo-name">Wireguard Access</div>
        </a>
        <!-- ========== List ============  -->
        <ul class="sidebar-list">
            <!-- -------- Non Dropdown List Item ------- -->
            <li>
                <div class="title">
                    <a href="#" class="link">
                        <i class='bx bx-grid-alt'></i>
                        <span class="name">Dashboard</span>
                    </a>
                    <!-- <i class='bx bxs-chevron-down'></i> -->
                </div>
                <div class="submenu">
                    <a href="#" class="link submenu-title">Dashboard</a>
                    <!-- submenu links here  -->
                </div>
            </li>

            <!-- -------- Dropdown List Item ------- -->
            <li class="dropdown">
                <div class="title">
                    <a href="#" class="link">
                        <i class='bx bx-collection'></i>
                        <span class="name">Category</span>
                    </a>
                    <i class='bx bxs-chevron-down'></i>
                </div>
                <div class="submenu">
                    <a href="#" class="link submenu-title">Category</a>
                    <a href="#" class="link">HTML & CSS</a>
                    <a href="#" class="link">JavaScript</a>
                    <a href="#" class="link">PHP & MySQL</a>
                </div>
            </li>

            <!-- -------- Non Dropdown List Item ------- -->
            <li>
                <div class="title">
                    <a href="#" class="link">
                        <i class='bx bx-line-chart'></i>
                        <span class="name">Analytics</span>
                    </a>
                    <!-- <i class='bx bxs-chevron-down'></i> -->
                </div>
                <div class="submenu">
                    <a href="#" class="link submenu-title">Analytics</a>
                    <!-- submenu links here  -->
                </div>
            </li>

            <!-- -------- Non Dropdown List Item ------- -->
            <li>
                <div class="title">
                    <a href="#" class="link">
                        <i class='bx bx-pie-chart-alt-2'></i>
                        <span class="name">Chart</span>
                    </a>
                    <!-- <i class='bx bxs-chevron-down'></i> -->
                </div>
                <div class="submenu">
                    <a href="#" class="link submenu-title">Chart</a>
                    <!-- submenu links here  -->
                </div>
            </li>

            <!-- -------- Dropdown List Item ------- -->
            <li class="dropdown">
                <div class="title">
                    <a href="#" class="link">
                        <i class='bx bx-extension'></i>
                        <span class="name">Plugins</span>
                    </a>
                    <i class='bx bxs-chevron-down'></i>
                </div>
                <div class="submenu">
                    <a href="#" class="link submenu-title">Plugins</a>
                    <a href="#" class="link">UI Face</a>
                    <a href="#" class="link">Pigments</a>
                    <a href="#" class="link">Box Icons</a>
                </div>
            </li>
 

            <!-- -------- Non Dropdown List Item ------- -->
            <li>
                <div class="title">
                    <a href="#" class="link">
                        <i class='bx bx-cog'></i>
                        <span class="name">Settings</span>
                    </a>
                    <!-- <i class='bx bxs-chevron-down'></i> -->
                </div>
                <div class="submenu">
                    <a href="#" class="link submenu-title">Settings</a>
                    <!-- submenu links here  -->
                </div>
            </li>
        </ul>
    </div>
    <!-- ============= Home Section =============== -->
    <section class="home">
        <div class="header-sidebar">
            <div class="toggle-sidebar"><i class='bx bx-menu'></i></div>
            <div class="user-info" id="user-info">
                <span class="material-icons">account_circle</span>
                <p class="username"><?= $currentUser['username'] ?></p>
            </div>
            <div class="user-info-menu" id="user-info-menu">
                <ul>
                    <li>
                        <a href="./login.php">Logout</a>
                        <span class="material-icons">logout</span>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Main -->
        <div class="container">
            <div class="content">
                <div class="tools-bar">
                    <!-- Search -->
                    <div class="search-tool tools">
                        <label for="search"><small>Search:</small></label>
                        <input type="text" class="txt-search" id="txt-search" placeholder="Search">
                    </div>
                    <!-- filter -->
                    <div class="filter-tool tools">
                        <label for="filter"><small>Filter:</small></label>
                        <div class="tunel-selecter">
                            <select id="tunel-selecter-tool-bar">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="item-container">
                    <!-- Loading page -->
                    <div class="loading-page" id="loading-page-data">
                        <img class="loader" src="./assets/img/logo1.jpg" alt="">
                    </div>
                    <!--  -->
                    <ul class="items-list" id="items-list"></ul>
                </div>
            </div>
            <div class="tools-container">
                <div class="top-tools-bar">
                    <div class="top-tools-bar-content">
                         <!-- Loading page -->
                         <div class="loading-page" id="loading-page-tunel">
                                <img class="loader" src="./assets/img/logo1.jpg" alt="">
                            </div>
                            <!--  -->
                        <h6 style="text-align: center; background: #f3f3f3; color: #555555;" class="mb-10 py-10">Tunel</h6>
                        <div class="top-tools-bar-left" id="top-tool-bar-left"></div>
                    </div>
                    <div class="top-tools-bar-right">
                        <div class="add-tool tools" id="add-tool">
                            <!-- Loading page -->
                            <div class="loading-page" id="loading-page-add-client">
                                <img class="loader" src="./assets/img/logo1.jpg" alt="">
                            </div>
                            <!--  -->
                            <h6 style="text-align: center; background: #f3f3f3; color: #555555;" class="mb-10 py-10">Add new client</h6>
                            <label for="add"><small>Client name:</small></label>
                            <div>
                                <input type="text" class="my-10" id="txt-add-client" style="width: 100%" placeholder="Enter name">
                            </div>
                            <label for="add"><small>Tunel:</small></label>
                            <div class="tunel-selecter">
                                <select id="tunel-selecter" class="my-10" style="width: 100%">
                                </select>
                            </div>
                            <button class="btn btn-blue my-10" id="btn-add-client">Add</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Link JS -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
    <script src="assets/js/main.js"></script>

</body>

</html>