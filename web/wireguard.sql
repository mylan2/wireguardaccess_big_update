-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 18, 2024 at 01:10 PM
-- Server version: 8.0.35-0ubuntu0.22.04.1
-- PHP Version: 8.1.2-1ubuntu2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wireguard`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `client_id` int NOT NULL,
  `client_name` varchar(50) NOT NULL,
  `interface_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `interface`
--

CREATE TABLE `interface` (
  `interface_id` int NOT NULL,
  `interface_name` varchar(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `interface`
--

INSERT INTO `interface` (`interface_id`, `interface_name`, `created_at`) VALUES
(1, 'wg0', '2024-01-12 08:26:33'),
(2, 'wg1', '2024-01-17 01:00:23'),
(3, 'wg2', '2024-01-17 01:00:23');

-- --------------------------------------------------------

--
-- Table structure for table `server_key`
--

CREATE TABLE `server_key` (
  `server_key_id` int NOT NULL,
  `server_private_key` varchar(255) NOT NULL,
  `server_public_key` varchar(255) NOT NULL,
  `interface_id` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `server_key`
--

INSERT INTO `server_key` (`server_key_id`, `server_private_key`, `server_public_key`, `interface_id`, `created_at`) VALUES
(38, 'QEvUElvCm1Tev3ubdjYN91nom2Mm3qU3ZWm1YBPifkg=', 'W4qh6L3+Gq3Jnic4GunTqV6lwgY3bLAu2t36000x52U=', 1, '2024-01-17 01:21:12'),
(39, 'eJWw4kB11JMZyv2sT3PduZSDYAB1Tn2gvR1CQCg8pF8=', 'XBUPBCnoi4DEPCyvvQf9h642e5i2n/h7UGaczC6cyTs=', 2, '2024-01-17 01:21:26'),
(41, 'oOEJNakSbWKAuHqWDGorxMP4OnxMwmDbY4bZ71rYJXA=', 'z6tD0kDoTVcvolsUQnCifDylyk3hD0umq7lGpjvXqW4=', 3, '2024-01-17 01:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `email`, `password`, `created_at`) VALUES
(1, 'vctrung', NULL, '202cb962ac59075b964b07152d234b70', '2024-01-12 02:17:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`,`client_name`),
  ADD KEY `interface_id` (`interface_id`);

--
-- Indexes for table `interface`
--
ALTER TABLE `interface`
  ADD PRIMARY KEY (`interface_id`);

--
-- Indexes for table `server_key`
--
ALTER TABLE `server_key`
  ADD PRIMARY KEY (`server_key_id`),
  ADD KEY `interface_id` (`interface_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `interface`
--
ALTER TABLE `interface`
  MODIFY `interface_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `server_key`
--
ALTER TABLE `server_key`
  MODIFY `server_key_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`interface_id`) REFERENCES `interface` (`interface_id`);

--
-- Constraints for table `server_key`
--
ALTER TABLE `server_key`
  ADD CONSTRAINT `server_key_ibfk_1` FOREIGN KEY (`interface_id`) REFERENCES `interface` (`interface_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
