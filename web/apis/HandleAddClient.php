<?php
include '../dbConnect.php';
$__conn = OpenCon();

if (isset($_POST)) {
    $__data = trim(file_get_contents("php://input"));
    $__arr = json_decode($__data, true);
    $__client_name = $__arr['name'];
    $__tunel = $__arr['tunel'];

    // Debug
    // $__client_name = 'tes2sd225';
    // $__tunel = 'wg01';

    $__clientExisted = shell_exec("sudo ls -1 /etc/wireguard/clients/ 2>&1");
    $__tunelExisted = shell_exec("sudo ls -1 /etc/wireguard/ | grep 'wg.*.conf' 2>&1");
    $__clientExisted = explode("\n", $__clientExisted);
    $__tunelExisted = explode(".conf\n", $__tunelExisted);
    $__error = false;
    $__message = "";
    // Debug
    // var_dump($__clientExisted); 
    // var_dump($__tunelExisted);
    // 
    $__status = null;
    $__result = null;
    // 

    if (strlen($__client_name) > 0 and strlen($__tunel) > 0) {

        if (!in_array($__client_name, $__clientExisted) and in_array($__tunel, $__tunelExisted)) {
            $__error = false;
        } else {
            $__error = true;
            $__message = "Client name is existed or tunel is not exsit";
            echo json_encode(
                [
                    'success' => false,
                    'message' => $__message
                ]
            );
        }
        if ($__error == false) { 
            exec('sudo bash /var/www/web/tools/wg_init.sh ' . $__client_name . ' ' . $__tunel, $__result, $__status);
            // Debug
            // $__tunel_config_file = shell_exec('sudo bash /var/www/web/tools/wg_init.sh ' . $__client_name . ' ' . $__tunel.' 2>&1');
            // echo $__status;
            if ($__status == 0) {

                switch ($__tunel) {
                    case 'wg0':
                        $__wg_id = '1';
                        $__server_private_key_file = 'server_' . $__tunel . '.key';
                        $__server_public_key_file = 'server_' . $__tunel . '.pub';
                        break;
                    case 'wg1':
                        $__wg_id = '2';
                        $__server_private_key_file = 'server_' . $__tunel . '.key';
                        $__server_public_key_file = 'server_' . $__tunel . '.pub';
                        break;
                    default:
                        $__wg_id = '3';
                        $__server_private_key_file = 'server_' . $__tunel . '.key';
                        $__server_public_key_file = 'server_' . $__tunel . '.pub';
                        break;
                }
                $__message = "Add Client successfully !!!";
                $__server_private_key = exec('sudo cat /etc/wireguard/' . $__server_private_key_file);
                $__server_public_key = exec('sudo cat /etc/wireguard/' . $__server_public_key_file);
                $__sql_select_key = "SELECT `server_private_key`, `server_public_key`, `interface_id` FROM `server_key` WHERE 1";

                if ($__result_sql_select_key = mysqli_query($__conn, $__sql_select_key)) {
                    if (mysqli_num_rows($__result_sql_select_key) > 0) {
                        $__private_key_exist = false;
                        while ($__row = mysqli_fetch_assoc($__result_sql_select_key)) {
                            if ($__row['server_private_key'] == $__server_private_key) {
                                $__private_key_exist = true;
                            }
                        }
                        if ($__private_key_exist == false) {
                            $__insert_Query = "INSERT INTO `server_key` (`server_key_id`, `server_private_key`, `server_public_key`, `interface_id`)  
                                            VALUES (NULL, ?, ?, ?)";
                            $__stmt = mysqli_prepare($__conn, $__insert_Query);
                            mysqli_stmt_bind_param($__stmt, "sss", $__server_private_key, $__server_public_key, $__wg_id);
                            $__insert_query_success = mysqli_stmt_execute($__stmt);
                            mysqli_stmt_close($__stmt);
                        }
                    } else {
                        $__insert_Query = "INSERT INTO `server_key` (`server_key_id`, `server_private_key`, `server_public_key`, `interface_id`)  
                                        VALUES (NULL, ?, ?, ?)";
                        $__stmt = mysqli_prepare($__conn, $__insert_Query);
                        mysqli_stmt_bind_param($__stmt, "sss", $__server_private_key, $__server_public_key, $__wg_id);
                        $__insert_query_success = mysqli_stmt_execute($__stmt);
                        mysqli_stmt_close($__stmt);
                    }
                    mysqli_free_result($__result_sql_select_key);
                    mysqli_close($__conn);
                    echo json_encode(
                        [
                            'success' => true,
                            'message' => $__message
                        ]
                    );
                } else {
                    $__message = "Failed client addition";
                    echo json_encode(
                        [
                            'success' => false,
                            'message' => $__message
                        ]
                    );
                }
            } else {
                echo json_encode(
                    [
                        'success' => false,
                        'message' => $__message
                    ]
                );
            }
        }
    }
}
