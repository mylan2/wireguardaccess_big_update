<?php 

if (isset($_POST)) {
    $__data = trim(file_get_contents("php://input"));
    $__data = json_decode($__data, true);
    $__name = $__data['tunel'];
    // 
    $__status = null;
    $__result = null;
    // 

    if (is_null($__name)) {
        echo json_encode(
            [
                'success' => false,
                'message' => "This tunel is not found !!!"
            ]
        );
    } else { 
        $__tunel_active = shell_exec("sudo wg | grep -o wg[0-9]");
        $__tunel_active = explode("\n",(string)$__tunel_active);
        if(in_array($__name, $__tunel_active)) {
            echo json_encode(
                [
                    'success' => false,
                    'message' => "This tunel is Running !!!",
                    'reload' => false
                ]
            );
        } else {
            exec('sudo wg-quick up ' . $__name, $__result, $__status);
            if ($__status == 0) {
                echo json_encode(
                    [
                        'success' => true,
                        'message' => "This tunel has been restarted !!!",
                        'reload' => true
                    ]
                );
            } else {
                echo json_encode(
                    [
                        'success' => false,
                        'message' => "Tunel start failure !!!",
                        'reload' => true
                    ]
                );
            } 
        }
         
    }
}
?>