<?php

$__tunel_config_file = shell_exec("sudo ls -1 /etc/wireguard/ | grep 'wg.*.conf' 2>&1");

if (empty($__tunel_config_file)) {
    echo json_encode(
        [
            'success' => false,
            'message' => 'No tunel found !!!'
        ]
    );
} else { 
    $__tunel_active = shell_exec("sudo wg | grep -o wg[0-9]");
    echo json_encode(
        [
            'success' => true,
            'tunels' => $__tunel_config_file,
            'tunels_active' => empty($__tunel_active) ? null : $__tunel_active
        ]
    );
}
