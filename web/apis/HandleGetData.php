<?php

$__data = shell_exec('sudo python3 /var/www/web/tools/script.py 2>&1');

// var_dump($__data);
// $clients = $__data->Clients; 
// $clients_config = $__data->Client_config;
// echo $__data;

if (empty($__data)) {
    $__message = "Some thing went wrong";
    echo json_encode(
        [
            'success' => false,
            'result' => $__message
        ]
    );
} else { 
    echo json_encode(
        [
            'success' => true,
            'result' => $__data
        ]
    );
}
