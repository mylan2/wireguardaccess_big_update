<?php
session_start();
if (empty($_SESSION['loggedin'])) {
    header("location:login.php");
    exit();
}
if (isset($_POST)) {
    $__data = trim(file_get_contents("php://input"));
    $__arr = json_decode($__data, true);
    $__client = $__arr['client'];
    $__clients = shell_exec('sudo ls -1 /etc/wireguard/clients/ | sort -V');
    $__clients = explode("\n", $__clients);
    if (in_array($__client, $__clients)) {
        $__file =  $__client . '/wg0.conf';
        $__status = null;
        $__result = null;
        $__config_file = shell_exec('sudo cat /etc/wireguard/clients/' . $__file . ' 2>&1');
        echo json_encode(
            [
                'client' => $__client,
                'success' => true,
                'result' => $__config_file
            ]
        );
    } else {
        echo json_encode(
            [
                'client' => $__client,
                'success' => false,
                'message' => 'Some thing went wrong !!!'
            ]
        );
    }
}
