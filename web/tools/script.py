
import json
import subprocess
from os import popen 


# - Parsing with command line
 

#subprocess.Popen('sudo ls /etc/wireguard/clients/ | tee /opt/wireguard/data.txt > /dev/null', shell=True)

__get_clients = popen("sudo ls -lt1 /etc/wireguard/clients/ | awk -F' ' '{print $9}'").read().rstrip().split('\n')
__get_clients = __get_clients[1:] 
# __get_clients_connected = popen('sudo wg | grep -e peer -e latest').read()
# print(__get_clients[0])
# print(__get_clients_wg0_config)
# print(__get_clients_connected)

__obj_client={} 

for __index, __value in enumerate(__get_clients):
    __obj_client_config={}
    __get_clients_wg0_config = popen('sudo cat /etc/wireguard/clients/'+ __value + '/wg0.conf').read().rstrip().split('\n')
    __obj_client_config['interface'] =  __get_clients_wg0_config[0]
    __obj_client_config['Address'] =  __get_clients_wg0_config[1].split("=")[1].rstrip()
    __obj_client_config['PrivateKey'] =  __get_clients_wg0_config[2].split("=")[1].rstrip()
    __obj_client_config['Peer'] =  __get_clients_wg0_config[3]
    __obj_client_config['PublicKey'] =  __get_clients_wg0_config[4].split("=")[1].rstrip()
    __obj_client_config['Endpoint'] =  __get_clients_wg0_config[5].split("=")[1].rstrip()
    __obj_client_config['AllowedIPs'] =  __get_clients_wg0_config[6].split("=")[1].rstrip()
    __obj_client_config['Persistentkeepalive'] =  __get_clients_wg0_config[7].split("=")[1].rstrip()
    __obj_client[__value] = __obj_client_config
     
 
# - Convert data to OBJ
__obj_Data = {
  "clients": __get_clients,
  "client_config": __obj_client,
}
# - Convert OBJ into JSON:
__json_Data = json.dumps(__obj_Data)
# - The result is a JSON string:
print(__json_Data)

