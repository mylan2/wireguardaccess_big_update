#!/bin/bash

# Define paths and variables
WG_CONFIG_PATH="/etc/wireguard"
WG_FILE_CONF=$2

if [ -e "$WG_CONFIG_PATH/$WG_FILE_CONF.conf" ]
    then
        case $WG_FILE_CONF in
            "wg0")
                NETWORK="172.16.0.0/16"
                GATEWAY="172.16.0.1/16"
                NET_ID="172.16."

                PREFIX_LENGTH='16'
                SERVER_PORT="51820" 
                SERVER_PUBLIC_KEY_FILE="server_$WG_FILE_CONF.pub"
                SERVER_PRIVATE_KEY_NAME="server_$WG_FILE_CONF.key"
                ;;
            "wg1")
                NETWORK="10.0.0.0/8"
                GATEWAY="10.0.0.1/8"
                PREFIX_LENGTH='8'
                NET_ID="10."

                SERVER_PORT="51821" 
                SERVER_PUBLIC_KEY_FILE="server_$WG_FILE_CONF.pub"
                SERVER_PRIVATE_KEY_NAME="server_$WG_FILE_CONF.key"
                ;;
            "wg2")
                NETWORK="10.10.0.0/24"
                GATEWAY="10.10.0.1/24"
                PREFIX_LENGTH='24'
                NET_ID="10.10.0."
                SERVER_PORT="51822" 
                SERVER_PUBLIC_KEY_FILE="server_$WG_FILE_CONF.pub"
                SERVER_PRIVATE_KEY_NAME="server_$WG_FILE_CONF.key"
                ;;
            *)
                NETWORK="10.0.0.0/8"
                GATEWAY="10.0.0.1/16"
                PREFIX_LENGTH='16'
                NET_ID="10.0.0."
                SERVER_PUBLIC_KEY_FILE="server_$WG_FILE_CONF.pub"
                SERVER_PRIVATE_KEY_NAME="server_$WG_FILE_CONF.key"
                ;;
        esac
fi
# Function to generate a new client configuration
generate_client_config() {
 
    CLIENT_NAME=$1
    # Generate client key pair
    CLIENT_PRIVATE_KEY=$(wg genkey)
    CLIENT_PUBLIC_KEY=$(echo "$CLIENT_PRIVATE_KEY" | wg pubkey)

    # Generate client configuration file
    CLIENT_CONF="$WG_CONFIG_PATH/clients/$CLIENT_NAME/wg0.conf"
    mkdir -p "$WG_CONFIG_PATH/clients/$CLIENT_NAME"
    touch "$CLIENT_CONF"
    chmod 600 "$CLIENT_CONF"

    wg genkey | tee "$WG_CONFIG_PATH/clients/$CLIENT_NAME/ClientPrivateKey" | wg pubkey > "$WG_CONFIG_PATH/clients/$CLIENT_NAME/ClientPublicKey"

    CLIENT_PUBLIC_KEY=$(cat "$WG_CONFIG_PATH/clients/$CLIENT_NAME/ClientPublicKey")
    CLIENT_PRIVATE_KEY=$(cat "$WG_CONFIG_PATH/clients/$CLIENT_NAME/ClientPrivateKey") 
    
    IP_ALLOWED_LIST=$(cat $WG_CONFIG_PATH/$WG_FILE_CONF.conf | grep $NET_ID | awk -F'=' '{print $2}' | awk -F'/' '{print $1}' | sort  )
    OCTET_4=$(tail -n 1 <<< $IP_ALLOWED_LIST | awk -F'.' '{print $4}' )

    case $PREFIX_LENGTH in
        "24")
            if [ $(($OCTET_4)) -lt 254 ]
                then
                    IP_NEXT=$(($OCTET_4 + 1))
                    CLIENT_ADDRESS=$NET_ID$IP_NEXT 
            fi
            ;;
        "16")
            if [ $OCTET_4 -lt 254 ]
            then
                OCTET_3=$(tail -n 1 <<< $IP_ALLOWED_LIST | awk -F'.' '{print $3}' ) 
                IP_NEXT=$(($OCTET_4 + 1))
                CLIENT_ADDRESS=$NET_ID$OCTET_3.$IP_NEXT 
            else
                OCTET_3=$(tail -n 1 <<< $IP_ALLOWED_LIST | awk -F'.' '{print $3}' )
                 
                if [ $(($OCTET_3)) -lt 254 ]
                    then
                        START_IP="10"
                        IP_NEXT=$(($OCTET_3 + 1))
                        CLIENT_ADDRESS=$NET_ID$IP_NEXT.$START_IP  
                fi
            fi
            ;;
        *)
            if [ $OCTET_4 -lt 254 ]
            then 
                OCTET_2=$(tail -n 1 <<< $IP_ALLOWED_LIST | awk -F'.' '{print $2}')
                OCTET_3=$(tail -n 1 <<< $IP_ALLOWED_LIST | awk -F'.' '{print $3}')
                IP_NEXT=$(($OCTET_4 + 1))
                CLIENT_ADDRESS=$NET_ID$OCTET_2.$OCTET_3.$IP_NEXT 
                 
            else
                OCTET_3=$(tail -n 1 <<< $IP_ALLOWED_LIST | awk -F'.' '{print $3}')
              
                if [ $(($OCTET_3)) -lt 254 ]
                then
                    OCTET_2=$(tail -n 1 <<< $IP_ALLOWED_LIST | awk -F'.' '{print $2}')
                    START_IP="10"
                    IP_NEXT=$(($OCTET_3 + 1))
                    CLIENT_ADDRESS=$NET_ID$OCTET_2.$IP_NEXT.$START_IP
                    
                else
                    OCTET_2=$(tail -n 1 <<< $IP_ALLOWED_LIST | awk -F'.' '{print $2}')
                    
                    if [ $(($OCTET_2)) -lt 254 ]
                    then
                        START_IP="10"
                        IP_NEXT=$(($OCTET_2 + 1))
                        OCTET_3=$(tail -n 1 <<< $IP_ALLOWED_LIST | awk -F'.' '{print $3}')
                        if [ $OCTET_3 -gt 253 ]
                        then
                            OCTET_3="0"
                        fi
                        CLIENT_ADDRESS=$NET_ID$IP_NEXT.$OCTET_3.$START_IP 
                    fi
                fi
            fi
            ;;
    esac
    
    echo "[Interface]" > "$CLIENT_CONF"
    echo "Address = $CLIENT_ADDRESS/$PREFIX_LENGTH" >> "$CLIENT_CONF"
    echo "PrivateKey = $CLIENT_PRIVATE_KEY" >> "$CLIENT_CONF"
    echo "[Peer]" >> "$CLIENT_CONF"
    echo "PublicKey = $SERVER_PUBLIC_KEY" >> "$CLIENT_CONF"
    echo "Endpoint = $SERVER_PUBLIC_IP:$SERVER_PORT" >> "$CLIENT_CONF"
    echo "AllowedIPs = $NETWORK" >> "$CLIENT_CONF"
    echo "PersistentKeepalive = 15" >> "$CLIENT_CONF"

    # # Update server
    echo "" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "[Peer]" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "PublicKey = $CLIENT_PUBLIC_KEY"  >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "AllowedIPs = $CLIENT_ADDRESS/32" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf" 
    wg set $WG_FILE_CONF peer $CLIENT_PUBLIC_KEY allowed-ips $CLIENT_ADDRESS 
    # # Restart server
    systemctl restart wg-quick@$WG_FILE_CONF

}

# Function to initialize WireGuard server
initialize_wireguard() {
    # get NIC
    NETWORK_INTERFACE_CARD=$(ip -o -4 route show to default | awk '{print $5}')
    # Initialize WireGuard interface
    wg genkey | tee "$WG_CONFIG_PATH/$SERVER_PRIVATE_KEY_NAME" | wg pubkey > "$WG_CONFIG_PATH/$SERVER_PUBLIC_KEY_FILE"
    chmod 600 "$WG_CONFIG_PATH/$SERVER_PRIVATE_KEY_NAME"
    chmod 600 "$WG_CONFIG_PATH/$SERVER_PUBLIC_KEY_FILE"

    # Set up server configuration
    echo "[Interface]" > "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "Address = $GATEWAY" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "ListenPort = $SERVER_PORT" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "PostUp = iptables -A FORWARD -i $WG_FILE_CONF -j ACCEPT; iptables -t nat -A POSTROUTING -o $NETWORK_INTERFACE_CARD -j MASQUERADE" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "PostDown = iptables -D FORWARD -i $WG_FILE_CONF -j ACCEPT; iptables -t nat -D POSTROUTING -o $NETWORK_INTERFACE_CARD -j MASQUERADE" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "PrivateKey = $(cat "$WG_CONFIG_PATH/$SERVER_PRIVATE_KEY_NAME")" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"

    # Run server interface
    systemctl enable wg-quick@$WG_FILE_CONF
    systemctl start wg-quick@$WG_FILE_CONF
}

# # Main script

# # Check if WireGuard is installed
if ! command -v wg &> /dev/null; then
    # echo "WireGuard is not installed. Please install WireGuard first."
    # exit 1
    apt update
    apt install wireguard
fi

# Check if running as root
if [ "$EUID" -ne 0 ]; then
    # echo "Please run this script as root or with sudo."
    exit 1
fi

# Check if WireGuard is already initialized
if ! [ -f "$WG_CONFIG_PATH/$WG_FILE_CONF.conf" ]; then
    initialize_wireguard
fi

# Get server public key and public IP
SERVER_PUBLIC_KEY=$(cat "$WG_CONFIG_PATH/$SERVER_PUBLIC_KEY_FILE")
SERVER_PUBLIC_IP="192.168.188.141"
# Allow adding clients
NAME=$1

generate_client_config "$NAME"


 
